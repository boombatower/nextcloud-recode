# nextcloud-recode

Simple utility to re-encode video files on a WebDAV endpoint (like Nextcloud).

- raw video files from most devices (phones, drones, security cameras) are relatively un-optimized (average 30-50% of orignal size when basic re-encoding) which leads to storage waste
- since a file server is not CPU intensive it is unlikely the re-encoding would make sense to be done on the same machine thus via WebDAV
- streaming files can only be done effectively with mp4 when moov atom is located at the front of the files

The intended usage is to run this tool in a loop (`LOOP=${minute_interval}`) to update files as they are uploaded. For an initial run it may make sense to expand the search window much further back to re-encode an entire server.


## considerations

WebDAV search does not provide a method for filtering by uploaded time which makes it impossible to effectively search for _new_ files. As such a local SQLite database is used to allow for very quick comparison against window of modified files (30 days by default) to determine new or modified files that should be checked.

In order to avoid overloading backup solutions a `BYTE_LIMIT` option is provided to limit the amount of change made in a `24 hour` period.


## improvement

Some potential areas for improvement:

- image re-encoding
- ffmpeg customization
- metrics export
