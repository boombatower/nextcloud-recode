FROM opensuse/leap:15.6

ENV DB_FILE="/var/recode.db"

RUN zypper -n ar -p 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_$releasever/' packman && \
    zypper -n --gpg-auto-import-keys ref --build-only && \
    zypper -n in ffmpeg-7 libjack0 python3-dataclasses python3-requests sqlite3 && \
    zypper clean --all

ADD recode.py /srv/

CMD ["python3", "-u", "/srv/recode.py"]
