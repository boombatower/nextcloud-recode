import os.path
import sqlite3
import subprocess
import tempfile
from dataclasses import dataclass
from datetime import datetime, timezone, timedelta
from sqlite3 import Connection
from time import sleep
from typing import List
from xml.etree import ElementTree as ET

import requests


@dataclass
class WebDAV:
    host: str
    prefix: str
    auth: tuple


@dataclass
class WebDAVFile:
    file_id: int
    href: str
    etag: str


def files_recent(web: WebDAV, path_filter: str, content_type: str, window: timedelta):
    url = f'{web.host}/{web.prefix}'
    response = requests.request(
        'SEARCH', url, auth=web.auth, headers={'Content-Type': 'text/xml'},
        data=search_xml(path_filter, content_type, window))
    response.raise_for_status()

    results = ET.fromstring(response.text)
    files = []
    for node in results:
        file_id = int(node.findtext('.//{http://owncloud.org/ns}fileid'))
        href = node.findtext('{DAV:}href')
        file = WebDAVFile(file_id, href, node.findtext('.//{DAV:}getetag').strip('"'))
        files.append(file)

        print(file.file_id, file.etag, file.href.replace(web.prefix + '/', ''))

    return files


def search_xml(path_filter: str, content_type: str, window: timedelta):
    since = datetime.now(timezone.utc) - window
    since_str = date_to_zulu(since)
    return f'''<?xml version="1.0" encoding="UTF-8"?>
<d:searchrequest xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns">
     <d:basicsearch>
         <d:select>
             <d:prop>
                 <oc:fileid/>
                 <d:getetag/>
             </d:prop>
         </d:select>
         <d:from>
             <d:scope>
                 <d:href>{path_filter}</d:href>
                 <d:depth>infinity</d:depth>
             </d:scope>
         </d:from>
         <d:where>
            <d:and>
                <d:eq>
                    <d:prop>
                        <d:getcontenttype/>
                    </d:prop>
                    <d:literal>{content_type}</d:literal>
                </d:eq>
                <d:gt>
                    <d:prop>
                        <d:getlastmodified/>
                    </d:prop>
                    <d:literal>{since_str}</d:literal>
                </d:gt>
            </d:and>
        </d:where>
        <d:orderby>
            <d:prop>
                <oc:fileid/>
            </d:prop>
            <d:ascending/>
        </d:orderby>
    </d:basicsearch>
</d:searchrequest>
    '''


def date_to_zulu(dt: datetime):
    if dt.tzinfo != timezone.utc:
        raise Exception('datetime must be in UTC')

    return dt.isoformat(timespec='seconds').replace('+00:00', 'Z')


def process(web: WebDAV, con: Connection, files: List[WebDAVFile],
            byte_interval: int, byte_limit: int, compression_min: float):
    processed = db_processed(con)
    print(f'loaded {len(processed)} previously processed file metadata')

    for file in files:
        byte_updated = db_byte_updated(con, byte_interval)
        if byte_updated >= byte_limit:
            print(f'process more bytes ({byte_updated}) than the {byte_interval} hour limit, stopping.')
            break

        if file.file_id in processed:
            if file.etag == processed[file.file_id]:
                print(f'already processed {file.href}')
                continue

            print(f'file etag changed from {processed[file.file_id]} to {file.etag}..reprocessing')

        print(f'process {file.href} ({byte_updated}/{byte_limit})')
        _, extension = os.path.splitext(file.href)
        with tempfile.NamedTemporaryFile(suffix=extension) as fp_in:
            with tempfile.NamedTemporaryFile(suffix=extension) as fp_out:
                result = process_file(web, file.href, fp_in, fp_out, compression_min)

        etag = file_etag(web, file.href)
        # TODO Given db_processed() limits the list if a file is reprocessed much later a file ID collision could occur.
        if file.file_id in processed:
            if 'before' in result:
                con.execute('UPDATE file SET etag = ?, before = ?, after = ? WHERE id = ?',
                            (etag, result.get('before'), result.get('after'), file.file_id))
            else:
                con.execute('UPDATE file SET etag = ? WHERE id = ?', (etag, file.file_id))
        else:
            con.execute('INSERT INTO file VALUES (?, ?, ?, ?, ?)',
                        (file.file_id, etag, result.get('before'), result.get('after'), datetime.now()))
        con.commit()


def process_file(web: WebDAV, path, fp_in, fp_out, compression_min: float):
    result = {}

    action = file_download(web, path, fp_in)
    if not action:
        return result

    if not recode(fp_in.name, fp_out.name, action):
        return result

    # Always track after size as part of bytes changed.
    result['after'] = os.path.getsize(fp_out.name)
    if action != 'moov':
        result['before'] = os.path.getsize(fp_in.name)
        ratio = result['after'] / result['before']

        if ratio > compression_min:
            print(f'compression of {ratio} not worth')
            return {}

        print(f'compression of {ratio} achieved')

    file_upload(web, path, fp_out.name)

    return result


def file_etag(web: WebDAV, path: str):
    url = f'{web.host}/{path}'
    response = requests.head(url, auth=web.auth)
    response.raise_for_status()
    return response.headers['ETag'].strip('"')


def file_download(web: WebDAV, path: str, fp):
    action = 'recode'

    url = f'{web.host}/{path}'
    with requests.get(url, auth=web.auth, stream=True) as r:
        r.raise_for_status()

        for chunk in r.iter_content(chunk_size=1024 * 5000):
            if chunk:
                fp.write(chunk)

            data = file_probe(fp.name)
            if data == 'moov':
                print('Move atom')
                action = 'moov'
            elif data:
                encoder = data.get('FORMAT').get('TAG:encoder')
                if encoder:
                    print(f'File has already been encoded by {encoder}!')
                    return None

            break

        # Download the rest of the file.
        for chunk in r.iter_content(chunk_size=None):
            if chunk:
                fp.write(chunk)

        fp.flush()

    return action


def file_probe(path):
    try:
        output = subprocess.check_output(['ffprobe', '-v', 'error', '-show_format', path], stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        print(e.stderr.decode())
        if 'moov atom not found' in e.stderr.decode():
            return 'moov'

        return None

    return probe_parse(output)


def probe_parse(output):
    section = None
    data = {}
    for line in output.decode().splitlines():
        if not section:
            if not line.startswith('['):
                continue

            section = line[1:-1]
            data.setdefault(section, {})
        else:
            if line.startswith('[/'):
                section_found = line[2:-1]
                if section_found != section:
                    raise Exception(f'section mismatch: {section_found} != {section}')

                section = None
            else:
                key, value = line.split('=', 1)
                data[section][key] = value

    return data


def recode(path_in, path_out, action):
    command = ['ffmpeg', '-y', '-i', path_in, '-movflags', '+faststart']
    if action == 'moov':
        command.extend(['-c', 'copy'])
    command.append(path_out)

    try:
        subprocess.check_call(command)
    except subprocess.CalledProcessError as e:
        print(e)
        return False

    return True


def file_upload(web: WebDAV, path: str, path_local: str):
    url = f'{web.host}/{path}'
    response = requests.put(url, auth=web.auth, data=open(path_local, 'rb'))
    response.raise_for_status()


def db_init(path):
    exists = os.path.exists(path)
    con = sqlite3.connect(path)

    if not exists:
        con.execute('CREATE TABLE file ('
                    'id INTEGER NOT NULL PRIMARY KEY,'
                    'etag text NOT NULL,'
                    'before INTEGER,'
                    'after INTEGER,'
                    'completed_at DATE NOT NULL)')

    return con


def db_processed(con: Connection, days = 60):
    after = datetime.now() + timedelta(days=-1 * days)
    processed = {}
    for row in con.execute('SELECT id, etag FROM file WHERE completed_at > ?', (after,)):
        processed[row[0]] = row[1]

    return processed


def db_byte_updated(con: Connection, since_hours=24):
    after = datetime.now() + timedelta(hours=-1 * since_hours)
    result = con.execute('SELECT SUM(after) FROM file WHERE completed_at > ?', (after,)).fetchone()[0]
    if not result:
        return 0

    return result


def main(host, prefix, user, password,
         path_filter, content_type, window: timedelta,
         db_file,
         byte_interval, byte_limit, compression_min, loop):
    print(f'process {content_type} on {host} in {path_filter} within {window.days} days')

    web = WebDAV(host, prefix, (user, password))
    con = db_init(db_file)

    interval = timedelta(minutes=loop)
    while True:
        files = files_recent(web, path_filter, content_type, window)
        process(web, con, files, byte_interval, byte_limit, compression_min)

        if loop < 0:
            break
        else:
            print(f'sleeping {interval}...')
            sleep(interval.total_seconds())


if __name__ == '__main__':
    main(
        os.getenv('WEBDAV_HOST'),
        os.getenv('WEBDAV_PREFIX'),
        os.getenv('WEBDAV_USER'),
        os.getenv('WEBDAV_PASS'),
        os.getenv('WEBDAV_SEARCH_PATH', '/'),
        os.getenv('WEBDAV_SEARCH_CONTENT_TYPE', 'video/mp4'),
        timedelta(days=int(os.getenv('WEBDAV_SEARCH_WINDOW', 30))),
        os.getenv('DB_FILE', '.files.db'),
        int(os.getenv('BYTE_INTERVAL', 24)),
        int(os.getenv('BYTE_LIMIT', 5 * 1024 ** 3)),
        float(os.getenv('COMPRESSION_MIN', 0.8)),
        int(os.getenv('LOOP', -1))
    )
